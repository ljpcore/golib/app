package app

import (
	"fmt"
	"io"
	"strings"
	"sync"
	"time"

	"gitlab.com/ljpcore/golib/test"
)

// Logger defines a common interface for types that can log informational
// messages.
type Logger interface {
	Write(format string, a ...interface{}) error
}

// --

// NullLogger is a logger that simply disregards any calls to Write.
type NullLogger struct{}

var _ Logger = &NullLogger{}

// NewNullLogger creates a new NullLogger.
func NewNullLogger() *NullLogger {
	return &NullLogger{}
}

// Write does nothing and always return a nil error.
func (l *NullLogger) Write(format string, a ...interface{}) error {
	return nil
}

// --

// StreamLogger is a logger that logs to a provided writer.
type StreamLogger struct {
	w io.Writer
}

var _ Logger = &StreamLogger{}

// NewStreamLogger creates a new logger that writes to the provided writer.
func NewStreamLogger(w io.Writer) *StreamLogger {
	return &StreamLogger{w: w}
}

// Write logs the given message to the underlying writer.
func (l *StreamLogger) Write(format string, a ...interface{}) error {
	_, err := fmt.Fprintf(l.w, format, a...)
	return err
}

// --

// PrefixLogger wraps an existing logger and prefixes any writes with a
// specified prefix.
type PrefixLogger struct {
	l      Logger
	prefix string
}

var _ Logger = &PrefixLogger{}

// NewPrefixLogger creates a new PrefixLogger with the provided underlying
// Logger.
func NewPrefixLogger(l Logger, prefix string) *PrefixLogger {
	return &PrefixLogger{
		l:      l,
		prefix: prefix,
	}
}

// Write prefixes the provided message and writes it to the underlying Logger.
func (l *PrefixLogger) Write(format string, a ...interface{}) error {
	return l.l.Write("%v%v", l.prefix, fmt.Sprintf(format, a...))
}

// --

// TimeLogger wraps an existing logger and prefixes any writes with the current
// time.
type TimeLogger struct {
	l        Logger
	format   string
	timezone *time.Location
	producer func() time.Time
}

var _ Logger = &TimeLogger{}

// NewTimeLogger creates a new TimeLogger with the provided format and timezone.
// Generally, you'll want to pass the provider function as time.Now().
func NewTimeLogger(l Logger, format string, timezone *time.Location, producer func() time.Time) *TimeLogger {
	return &TimeLogger{
		l:        l,
		format:   format,
		timezone: timezone,
		producer: producer,
	}
}

// Write prefixes the provided message and writes it to the underlying Logger.
func (l *TimeLogger) Write(format string, a ...interface{}) error {
	return l.l.Write("[%v] %v", l.producer().In(l.timezone).Format(l.format), fmt.Sprintf(format, a...))
}

// --

// MemoryLogger records all writes for later inspection.  Used for testing.
type MemoryLogger struct {
	writes []string
	mx     *sync.RWMutex
}

var _ Logger = &MemoryLogger{}

// NewMemoryLogger creates a new MemoryLogger.
func NewMemoryLogger() *MemoryLogger {
	return &MemoryLogger{
		mx: &sync.RWMutex{},
	}
}

// Write stores the formatted log message.
func (l *MemoryLogger) Write(format string, a ...interface{}) error {
	l.mx.Lock()
	defer l.mx.Unlock()

	l.writes = append(l.writes, fmt.Sprintf(format, a...))
	return nil
}

// HasExact fails the provided T if the logger hasn't written exactly the
// provided message.
func (l *MemoryLogger) HasExact(t test.TLike, formattedMessage string) {
	l.Has(t, func(x string) bool { return x == formattedMessage })
}

// Has fails the provided T if no logged messages pass the provided comparison
// function.
func (l *MemoryLogger) Has(t test.TLike, compare func(string) bool) {
	l.mx.RLock()
	defer l.mx.RUnlock()

	for _, write := range l.writes {
		if compare(write) {
			return
		}
	}

	sb := strings.Builder{}
	sb.WriteString("No logged messages passed comparison.  Messages that were logged:\n\n")

	for _, write := range l.writes {
		sb.WriteString(write)
		sb.WriteString("\n\n")
	}

	t.Fatalf(sb.String())
}

// Count returns the number of messages logged.
func (l *MemoryLogger) Count() int {
	return len(l.writes)
}
