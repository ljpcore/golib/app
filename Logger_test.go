package app

import (
	"bytes"
	"strings"
	"testing"
	"time"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestNullLoggerDoesNothing(t *testing.T) {
	// Arrange.
	l := NewNullLogger()

	// Act.
	err := l.Write("Hello, World!\n")

	// Assert.
	test.That(t, err, is.Nil)
}

func TestStreamLoggerWritesToWriter(t *testing.T) {
	// Arrange.
	buf := bytes.NewBuffer([]byte{})
	l := NewStreamLogger(buf)

	// Act.
	err := l.Write("Hello, %v!", "World")

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, string(buf.Bytes()), is.EqualTo("Hello, World!"))
}

func TestPrefixLoggerPrefixesCorrectly(t *testing.T) {
	// Arrange.
	buf := bytes.NewBuffer([]byte{})
	l := NewPrefixLogger(NewStreamLogger(buf), "[Server] ")

	// Act.
	err := l.Write("Hello, %v!", "World")

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, string(buf.Bytes()), is.EqualTo("[Server] Hello, World!"))
}

func TestTimeLoggerPrefixesCorrectly(t *testing.T) {
	// Arrange.
	formatStr := "2006/01/02 03:04 PM"
	provider := func() time.Time {
		return time.Date(2020, 1, 3, 5, 45, 0, 0, time.UTC)
	}

	buf := bytes.NewBuffer([]byte{})
	l := NewTimeLogger(NewStreamLogger(buf), formatStr, time.UTC, provider)

	// Act.
	err := l.Write("Hello, %v!", "World")

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, string(buf.Bytes()), is.EqualTo("[2020/01/03 05:45 AM] Hello, World!"))
}

func TestMemoryLoggerHas(t *testing.T) {
	// Arrange.
	l := NewMemoryLogger()
	l.Write("Hello, World!")
	l.Write("Hello, Again!")

	// Act.
	m1 := test.NewTMock()
	m2 := test.NewTMock()
	m3 := test.NewTMock()
	m4 := test.NewTMock()

	l.HasExact(m1, "Hello, World!")
	l.HasExact(m2, "Hello, Everyone!")
	l.Has(m3, func(x string) bool { return strings.HasSuffix(x, "Again!") })
	l.Has(m4, func(x string) bool { return strings.HasSuffix(x, "Everyone!") })

	// Assert.
	test.That(t, l.Count(), is.EqualTo(2))
	test.That(t, m1.FatalMessage, is.EqualTo(""))
	test.That(t, m2.FatalMessage, is.NotEqualTo(""))
	test.That(t, m3.FatalMessage, is.EqualTo(""))
	test.That(t, m4.FatalMessage, is.NotEqualTo(""))
}
