package app

import "context"

// Service represents a long-running service that is critical to the operation
// of the program.
type Service interface {
	// Returns a human-readable name for the service.  Used purely for
	// information purposes.
	Name() string

	// Runs the service.  Should not be called directly.  Must exit as soon as
	// possible if the provided context ends.  Arg is provided by the calling
	// App at runtime.
	Run(ctx context.Context, logger Logger, arg interface{}) error
}
