![](icon.png)

# app

A runtime framework for applications with background services.

---

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).
