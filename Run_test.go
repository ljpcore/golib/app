package app

import (
	"context"
	"errors"
	"testing"
	"time"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func SetupRunTest(failsSetup bool, logsAtTeardown bool, arg interface{}, services []Service) (*TestApp, *MemoryLogger, context.Context, context.CancelFunc) {
	logger := NewMemoryLogger()
	ctx, cancel := context.WithCancel(context.Background())

	return NewTestApp(failsSetup, logsAtTeardown, arg, services, logger), logger, ctx, cancel
}

func TestRunExitsWhenSetupFails(t *testing.T) {
	// Arrange.
	app, logger, ctx, cancel := SetupRunTest(true, false, nil, nil)

	// Act.
	err := runInContext(ctx, cancel, app)

	// Assert.
	test.That(t, logger.Count(), is.EqualTo(0))
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("error during setup: failed setup"))
}

func TestRunExitsWhenNoServices(t *testing.T) {
	// Arrange.
	app, logger, ctx, cancel := SetupRunTest(false, false, nil, nil)

	// Act.
	err := runInContext(ctx, cancel, app)

	// Assert.
	test.That(t, logger.Count(), is.EqualTo(0))
	test.That(t, err, is.Nil)
}

func TestRunExitsCleanlyWhenContextCancelled(t *testing.T) {
	// Arrange.
	services := []Service{NewTimeoutService("Timeout1", 1000*time.Millisecond)}
	app, logger, ctx, cancel := SetupRunTest(false, true, nil, services)

	// Act.
	go func() {
		time.Sleep(time.Millisecond * 100)
		cancel()
	}()

	err := runInContext(ctx, cancel, app)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, logger.Count(), is.EqualTo(6))

	logger.HasExact(t, "Starting 1 services...\n")
	logger.HasExact(t, "Started service 'Timeout1' successfully!\n")
	logger.HasExact(t, "Service 'Timeout1' stopped gracefully.\n")
	logger.HasExact(t, "Waiting for 1s\n")
	logger.HasExact(t, "Termination signal received, attempting tidy shutdown...\n")
	logger.HasExact(t, "Teardown!\n")
}

func TestRunExitsWhenServiceReturnsError(t *testing.T) {
	// Arrange.
	services := []Service{
		NewTimeoutService("Timeout1", 100*time.Millisecond),
		NewTimeoutService("Timeout2", 1000*time.Millisecond),
	}

	app, logger, ctx, cancel := SetupRunTest(false, false, nil, services)

	// Act.
	err := runInContext(ctx, cancel, app)

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("timeout ended"))
	test.That(t, logger.Count(), is.EqualTo(7))

	logger.HasExact(t, "Starting 2 services...\n")
	logger.HasExact(t, "Started service 'Timeout1' successfully!\n")
	logger.HasExact(t, "Started service 'Timeout2' successfully!\n")
	logger.HasExact(t, "Service 'Timeout2' stopped gracefully.\n")
	logger.HasExact(t, "Service 'Timeout1' failed with an error: timeout ended\n")
	logger.HasExact(t, "Waiting for 100ms\n")
	logger.HasExact(t, "Waiting for 1s\n")
}

// --

type TestApp struct {
	failsSetup     bool
	logsAtTeardown bool
	arg            interface{}
	services       []Service
	logger         Logger
}

var _ App = &TestApp{}

func NewTestApp(failsSetup bool, logsAtTeardown bool, arg interface{}, services []Service, logger Logger) *TestApp {
	return &TestApp{
		failsSetup:     failsSetup,
		logsAtTeardown: logsAtTeardown,
		arg:            arg,
		services:       services,
		logger:         logger,
	}
}

func (a *TestApp) Services() []Service {
	return a.services
}

func (a *TestApp) Setup(ctx context.Context) (Logger, interface{}, error) {
	if a.failsSetup {
		return nil, nil, errors.New("failed setup")
	}

	return a.logger, nil, nil
}

func (a *TestApp) Teardown(logger Logger, err error) {
	if a.logsAtTeardown {
		logger.Write("Teardown!\n")
	}
}

// --

type TimeoutService struct {
	name    string
	timeout time.Duration
}

var _ Service = &TimeoutService{}

func NewTimeoutService(name string, timeout time.Duration) *TimeoutService {
	return &TimeoutService{
		name:    name,
		timeout: timeout,
	}
}

func (s *TimeoutService) Name() string {
	return s.name
}

func (s *TimeoutService) Run(ctx context.Context, logger Logger, arg interface{}) error {
	logger.Write("Waiting for %v\n", s.timeout)
	timer := time.NewTimer(s.timeout)

	select {
	case <-ctx.Done():
		return nil
	case <-timer.C:
		timer.Stop()
		return errors.New("timeout ended")
	}
}
