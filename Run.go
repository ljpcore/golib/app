package app

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Run runs the provided App.  Should be called directly in main.
func Run(app App) {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		<-sigc
		cancel()
	}()

	if err := runInContext(ctx, cancel, app); err != nil {
		fmt.Fprintf(os.Stderr, "FATAL: %v\n", err)
		os.Exit(1)
	}
}

func runInContext(ctx context.Context, cancel context.CancelFunc, app App) error {
	// Setup the application and get the logger we'll use.
	logger, arg, err := app.Setup(ctx)
	if err != nil {
		return fmt.Errorf("error during setup: %w", err)
	}

	// If we aren't given a logger, create a basic one to use.
	if logger == nil {
		streamLogger := NewStreamLogger(os.Stdout)
		logger = NewTimeLogger(streamLogger, time.RFC822, time.Local, time.Now)
	}

	// Get the services we'll be running.  If there aren't any, we just stop
	// here.
	services := app.Services()
	serviceCount := len(services)
	if serviceCount < 1 {
		return nil
	}

	// Start the services and prepare a channel for their error results.
	logger.Write("Starting %v services...\n", serviceCount)
	errc := make(chan error, serviceCount)
	for _, service := range services {
		startService(ctx, errc, service, logger, arg)
	}

	// Wait for our first error, or the context to end.  If we receive an error
	// first, we store it so we can report it as the originating fault, then we
	// cancel the context.  If the context ends first, we're doing a graceful
	// shutdown.
	var terminationError error
	errorCount := 0

	select {
	case <-ctx.Done():
		logger.Write("Termination signal received, attempting tidy shutdown...\n")
	case err := <-errc:
		terminationError = err
		errorCount++
		cancel()
	}

	// Receive all the errors to ensure all the services have shutdown.
	for errorCount < serviceCount {
		<-errc
		errorCount++
	}

	// Close the error channel and run the application teardown.
	close(errc)
	app.Teardown(logger, terminationError)

	// Return the termination error, if there was one.
	return terminationError
}

func startService(ctx context.Context, errc chan<- error, service Service, logger Logger, arg interface{}) {
	serviceName := service.Name()
	logger.Write("Started service '%v' successfully!\n", serviceName)

	go func() {
		err := service.Run(ctx, logger, arg)

		if err != nil {
			logger.Write("Service '%v' failed with an error: %v\n", serviceName, err)
		} else {
			logger.Write("Service '%v' stopped gracefully.\n", serviceName)
		}

		errc <- err
	}()
}
