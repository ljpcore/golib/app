package app

import "context"

// App represents a runtime application with a number of background services.
type App interface {
	// Returns the set of services to run for this application.
	Services() []Service

	// Setup allows for any logic that must be run before services are started,
	// as well as expecting creation of a Logger and optionally an interface{}
	// arg to pass to services.  If a nil Logger is returned, the App will
	// create it's own to use.  If a non-nil error is returned, the app exits
	// immediately.  The base context of the app is passed to Setup also.
	Setup(ctx context.Context) (Logger, interface{}, error)

	// Teardown is called after all services have stopped.  If err is nil, the
	// application shutdown gracefully due to a SIGTERM/SIGINT.  If err is
	// non-nil, it is the error that caused the application to stop.
	Teardown(logger Logger, err error)
}
